# Payum EveryPay module by Omnibuy - usage example with Symfony

## Introduction
The purpose of this example project is to show how to use the `omnibuy/payum-everypay` module correctly with 
Symfony and templates.

## Usage

- `composer install`
- Set the config parameters in `.env`
- `php bin/console doctrine:migrations:migrate`
- `php bin/console server:run`

## License

Project is released under the [MIT License](LICENSE).
