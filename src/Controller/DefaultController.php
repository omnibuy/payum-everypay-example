<?php

namespace App\Controller;

use App\Entity\Payment;
use Payum\Core\Payum;
use Payum\Core\Request\GetHumanStatus;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends AbstractController
{
    public function index()
    {
        return $this->render('index.html.twig', ['number' => 1]);
    }

    public function startPayment(Payum $payum)
    {
        $storage = $payum->getStorage(Payment::class);

        $payment = $storage->create();
        $payment->setNumber(uniqid());
        $payment->setCurrencyCode('EUR');
        $payment->setTotalAmount(123); // 1.23 EUR
        $payment->setDescription('A description');
        $payment->setClientId('anId');
        $payment->setClientEmail('foo@example.com');

        $storage->update($payment);

        $gatewayName = 'everypay';
        $captureToken = $payum->getTokenFactory()->createCaptureToken(
            $gatewayName,
            $payment,
            'thank_you'
        );

        return $this->redirect($captureToken->getTargetUrl());
    }

    public function notify(Request $request, Payum $payum)
    {
        $orderRefernce = $request->query->get('order_reference');
        $paymentReference = $request->query->get('payment_reference');

        $storage = $payum->getStorage(Payment::class);

        /** @var Payment $payment */
        $payments = $storage->findBy(['number' => $orderRefernce]);

        if (!isset($payments[0])) {
            throw new NotFoundHttpException();
        }

        $paymentDetails = $payments[0]->getDetails();
        if (!isset($paymentDetails['notification_url'])) {
            throw new NotFoundHttpException();
        }

        return RedirectResponse::create($paymentDetails['notification_url'] . '?payment_reference=' . $paymentReference);
    }

    public function thankYou(Request $request, Payum $payum)
    {
        $token = $payum->getHttpRequestVerifier()->verify($request);
        $gateway = $payum->getGateway($token->getGatewayName());
        $gateway->execute($status = new GetHumanStatus($token));

        return $this->render('thank_you.html.twig', ['status' => $status]);
    }
}
